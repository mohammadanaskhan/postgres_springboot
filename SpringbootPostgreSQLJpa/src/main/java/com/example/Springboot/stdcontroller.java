package com.example.Springboot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class stdcontroller {

	@Autowired
	private studentrepo stdre;
	
	@PostMapping("/add")
	public student stdcreate(@RequestBody student std1) {
		return this.stdre.save(std1);
	}
	@GetMapping("/")
	public List<student> stdadd() {
		return this.stdre.findAll();
	}
	
	@GetMapping("/view/{id}")
	public ResponseEntity<student> getstdbyid(@PathVariable(value="id") Integer id){
		student std12=stdre.findById(id).orElse(null);
		return ResponseEntity.ok().body(std12);
		
	}

	@PutMapping("/up/{id}")
	public ResponseEntity<student> updatestd(@PathVariable(value="id") Integer id, @Validated @RequestBody student studentDetails){
		student std12=stdre.findById(id).orElse(null);
		std12.setName(studentDetails.getName());
		std12.setPassword(studentDetails.getPassword());
		std12.setLname(studentDetails.getLname());
		return ResponseEntity.ok(this.stdre.save(std12));
		
	}
	
	@DeleteMapping("/del/{id}")
	public Map<String,Boolean> delstd(@PathVariable(value="id") Integer id){
		student std12=stdre.findById(id).orElse(null);
		this.stdre.delete(std12);
		Map<String, Boolean> mp=new HashMap<>();
		mp.put("Deleted", Boolean.TRUE);
		return mp;
		
	}
//	@Value("${spring.name}")
//	public String na;
//	
//	@GetMapping("/evn1")
//	public String val() {
//		return na;
//		
//	}
//	
//	@Value("${chk}")
//	public String na1;
//	
//	@GetMapping("/chk")
//	public String val1() {
//		return na1;
//		
//	}
}
